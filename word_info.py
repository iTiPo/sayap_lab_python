class WordInfo(object):
    def __init__(self, word, number=1):
        self.word = word
        self.number = number

    def __str__(self):
        return '{0} - {1}'.format(self.word, self.number)
