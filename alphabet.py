from word_info import WordInfo


class Alphabet(object):

    def __init__(self):
        self.abc = []

    #input words from file
    def input(self, words_file):
        self.abc = words_file.read().replace('\n', ' ').upper().split(' ')
        words_file.close()

    #count original words
    def count_words(self):
        words_number = len(self.abc)
        if words_number < 1:
            return 0

        #Replace self.abc content by WordInfo elements
        self.abc[0] = WordInfo(self.abc[0])
        original_words_number = 1
        current_word_position = 1

        while current_word_position < words_number:
            if self.abc[current_word_position] == self.abc[original_words_number - 1].word:
                self.abc[original_words_number - 1].number += 1
            else:
                original_words_number += 1
                self.abc[original_words_number - 1] = WordInfo(self.abc[current_word_position])
            current_word_position += 1

        #remove not original words
        current_word_position -= 1
        while current_word_position >= original_words_number:
            self.abc.pop(current_word_position)
            current_word_position -= 1

    #print words and it's number
    def print_result(self, result_file, number=-1):
        if number == -1:
            number = len(self.abc)

        for word_info_position in range(number):
            result_file.write(self.abc[word_info_position].__str__() + '\n')

    def prepare_to_print(self):
        #abc contains only words now
        self.abc.sort()
        self.count_words()
        #abc contains words and number for each now
        self.abc.sort(key=lambda word_info: word_info.number, reverse=True)
