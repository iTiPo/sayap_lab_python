from alphabet import Alphabet
import time
import sys

if __name__ == '__main__':

    if len(sys.argv) != 3 or len(sys.argv) != 4:
        print 'Some error with arguments. Please pass to a program words file and result file names.'
        sys.exit()

    words_file_path = sys.argv[1]
    result_file_path = sys.argv[2]

    abc = Alphabet()

    words_file = open(words_file_path, 'r')
    abc.input(words_file)
    words_file.close()

    start_time = time.time()
    abc.prepare_to_print()
    elapsed_time = time.time() - start_time
    result_file = open(result_file_path, 'w')
    result_file.write('{0} seconds\n'.format(elapsed_time))

    if (len(sys.argv) == 4):
        words_number = int(sys.argv[3])
    else:
        words_number = -1

    abc.print_result(result_file, words_number)

    result_file.close()
